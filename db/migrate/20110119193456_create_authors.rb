class CreateAuthors < ActiveRecord::Migration
  def self.up
    create_table :authors do |t|
      t.string :name
      t.string :email

      t.timestamps
    end
    add_index :authors, [:name, :email]
  end

  def self.down
    drop_table :authors
  end
end
