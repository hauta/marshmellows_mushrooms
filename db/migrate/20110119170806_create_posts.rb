class CreatePosts < ActiveRecord::Migration
  def self.up
    create_table :posts do |t|
      t.string :title
      t.text :content

      t.timestamps
    end
    add_index :posts, :title
  end

  def self.down
    drop_table :posts
  end
end
