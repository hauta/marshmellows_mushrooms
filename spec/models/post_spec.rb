require 'spec_helper'

describe Post do
  
  describe 'creation' do
    
    before(:each) do
      @attr = { :title => 'title', :content => 'content' }
    end
    
    it "should save properly" do
      lambda {
        Post.create(@attr)
      }.should change(Post, :count).by(1)
    end
  end

  describe 'validations' do
    
    before(:each) do
      @p = Post.new
    end
    
    it 'should prohibit post creation without a title' do
      @p.content = 'odoera'
      @p.should_not be_valid
    end
    
    it 'should prohibit post creation without content' do
      @p.title = 'odours'
      @p.should_not be_valid
    end

    it "should pass if a title and content was given" do
      @p.title = 'titlar'
      @p.content = 'como es'
      @p.should be_valid
    end
  end
  
  describe 'associations' do
    
    it "should have comments" do
      p = Post.new
      p.should respond_to(:comments)
    end
    
    it "should delete comments associated with a post" do
      p = Post.create(:title => 'hey oy!', :content => 'how bugs get to beds')
      c = Comment.create(:body => 'audo', :post => p)
      lambda {
        lambda {
          p.destroy
        }.should change(Comment, :count).by(-1)
      }.should change(Post, :count).by(-1)
    end
    
    it "should have an author" do
      p = Post.new
      p.should respond_to(:author)
    end
  end
end
