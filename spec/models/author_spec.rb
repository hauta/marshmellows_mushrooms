require 'spec_helper'

describe Author do
  
  it "should have a name" do
    a = Author.new
    a.should_not be_valid
  end
  
  it "should save correctly" do
    lambda {
      Author.create(:name => 'sweet', :email => 'sweet@leg.it')
    }.should change(Author, :count).by(1)
  end
  
  it "should have posts" do
    a = Author.new
    a.should respond_to(:posts)
  end
  
  it "should delete posts associated with an author" do
     a = Author.create(:name => 'obi', :email => 'ken@obi.wan')
     p = Post.create(:title => 'skorsee', :content => 'Ai leaving', :author => a)
     lambda {
       lambda {
         a.destroy
       }.should change(Post, :count).by(-1)
     }.should change(Author, :count).by(-1)
   end
end
