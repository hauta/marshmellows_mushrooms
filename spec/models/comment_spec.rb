require 'spec_helper'

describe Comment do
  
  before(:each) do
    @valid_attrs = {
      :body => 'This comment here.'
    }
  end
  
  it "should have a body" do
    c = Comment.new
    c.should_not be_valid
  end
  
  it "should save correctly" do
    lambda {
      Comment.create(@valid_attrs)
    }.should change(Comment, :count).by(1)
  end
  
  it "belongs to a post" do
    c = Comment.new
    c.should respond_to(:post)
  end
end
